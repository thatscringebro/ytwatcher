# YTwatcher

## Description

This simple script uses yt-dlp to automatically download the latest videos of your favorite youtubers. Just add the channels to the input file and run the src/ytwatcher.sh script to automatically download all videos from the last 2 days that haven't been already downloaded.

## Usage
* Make sure yt-dlp is installed on your system
* Open a terminal in the main folder
* Run the 'ytwatcher.sh' script using the following command
```bash
sh src/ytwatcher.sh
```
The script will automatically create every file and folder needed. The videos are downloaded to a downloads folder that is created if it doesn't exist

You may specify a custom download path when you run the script like this
```bash
sh src/ytwatcher.sh -d your/custom/path
```

## Setup
A setup script exists that will provide you a way to automatically download new videos every day at midnight. It's usage is very simple, simply run the 'setup.sh' script like this:
```bash
sh src/setup.sh
```
When the setup prompts you for channel names, please provide only the handle like this: https://youtube.com/@ {*only this part is needed*}


## Developper
|Name|GitLab|
|--|--|
|Merlin Gelinas|thatscringebro|

<sub>This script is entirely free of use, please share it! =^◡^=</sub>
