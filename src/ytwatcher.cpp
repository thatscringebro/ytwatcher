#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <sstream>

using namespace std;

// Function to run yt-dlp as a subprocess
void runYTDLP(const string& channelURL, const string& outputDirectory) {
    string cmd = "yt-dlp --dateafter now-5days --playlist-end 10 -o '" + outputDirectory + "/%(title)s.%(ext)s' " + channelURL;
    int result = system(cmd.c_str());
    if (result == 0) {
        cout << "Downloaded from: " << channelURL << endl;
    } else {
        cerr << "Error downloading from: " << channelURL << endl;
    }
}

int main() {
    // Load the list of YouTube channels from a file
    ifstream channelListFile("../files/channels");
    if (!channelListFile.is_open()) {
        cerr << "Failed to open channel list file." << endl;
        return 1;
    }

    // Set the output directory
    string outputDirectory = "../files/downloads";

    string channelURL;
    while (getline(channelListFile, channelURL)) {
        runYTDLP(channelURL, outputDirectory);
    }

    channelListFile.close();

    return 0;
}
